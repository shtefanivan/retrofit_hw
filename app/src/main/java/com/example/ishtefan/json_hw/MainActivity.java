package com.example.ishtefan.json_hw;

/*
1. Используя сервис https://restcountries.eu/ создать приложение
        "Каталог стран". Приложение должно уметь показывать список частей
        света (regions) частей частей света (sub regions) и, собственно,
        стран. Должна быть возможность просмотра информации по стране
        (столица, население, языки и пр. 5-7 пунктов).
*/


import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import java.io.Serializable;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class MainActivity extends AppCompatActivity {


    RecyclerView list;
//    public ArrayList<String> regions;
private RecyclerViewAdapter adapter;
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        list = (RecyclerView) findViewById(R.id.recycleview);
        LinearLayoutManager layoutManager = new LinearLayoutManager(MainActivity.this);
        list.setLayoutManager(layoutManager);

    }

    @Override
    protected void onResume() {
        super.onResume();

        Retrofit.getAllCountry(new Callback<List<Country>>() {
                                   @Override
                                   public void success(final List<Country> countries, Response response) {

                                       Collections.sort(countries, new Comparator<Country>() {
                                           public int compare(Country o1, Country o2) {
                                               return o1.region.toString().compareTo(o2.region.toString());
                                           }
                                       });

                                       String prevItem;
                                       String currItem;

                                       Iterator<Country> iterator = countries.iterator();
                                       prevItem = iterator.next().region;
                                       while (iterator.hasNext()) {
                                           currItem = iterator.next().region;
                                           if (prevItem.equals(currItem)) {
                                               iterator.remove();
                                           }
                                           prevItem = currItem;
                                       }
                                       adapter = new RecyclerViewAdapter(countries, MainActivity.this);
                                       list.setAdapter(adapter);

                                       final Intent intent = new Intent(MainActivity.this, CountryActivity.class);


                                       adapter.setListener(new RecyclerViewAdapter.OnRecyclerItemClickListener() {
                                           @Override
                                           public void onItemClickListener(Country item, int position) {
                                               Toast.makeText(MainActivity.this, item.region, Toast.LENGTH_SHORT).show();
                                               intent.putExtra("item", (Serializable) item);

                                               startActivity(intent);
                                           }
                                       });


                                       Toast.makeText(MainActivity.this, countries.get(1).region, Toast.LENGTH_SHORT).show();
                                   }

                                   @Override
                                   public void failure(RetrofitError error) {
                                       Toast.makeText(MainActivity.this, "ошибка", Toast.LENGTH_SHORT).show();
                                   }

                               }


        );


    }
}
