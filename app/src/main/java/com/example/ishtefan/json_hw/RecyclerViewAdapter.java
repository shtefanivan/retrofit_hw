package com.example.ishtefan.json_hw;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

/**
 * Created by ishtefan on 08.10.2016.
 */

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {

    private List<Country> strings;
    private Context context;
    private OnRecyclerItemClickListener listener;

    public RecyclerViewAdapter(List<Country> strings, Context context) {
        this.strings = strings;
        this.context = context;
    }

    public interface OnRecyclerItemClickListener {
        void onItemClickListener(Country item, int position);
    }

    public void setListener(OnRecyclerItemClickListener listener) {
        this.listener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(R.layout.item, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.tvItem.setText(strings.get(position).region);
    }

    @Override
    public int getItemCount() {
        return strings.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvItem;

        public ViewHolder(View item) {
            super(item);
            tvItem = (TextView) item.findViewById(R.id.item);
        }
    }

}

